#!/usr/bin/with-contenv bash

# shellcheck shell=bash
# shellcheck disable=SC2016
# shellcheck disable=SC1004
# shellcheck disable=SC2002
# shellcheck disable=SC1083
# shellcheck disable=SC2001
# shellcheck disable=SC2086

#####################################################
# CONFIG
#####################################################
ENDDIR=/data

#####################################################
# END CONFIG
#####################################################
IFS=$'\n'
for i in "$@"; do
    case ${i} in
    -t | --twitch)
        TWITCH="true"
        ;;
    -y | --youtube)
        YOUTUBE="true"
        ;;
    -d | --deezer)
        DEEZER="true"
        ;;
    -ig | --instagram)
		INSTAGRAM="true"
		;;
    -p | --pornhub)
		PORNHUB="true"
		;;
    *)
        TWITCH="true"
        YOUTUBE="true"
        ;;
    esac
done
log() {
    echo "[${1}] ${2}" >> "/var/log/social-dumper/${3}.log"
}
while true; do
    if [ "${YOUTUBE}" == "true" ]; then
        if [ -f /config/lists/youtube.json ]; then
            cat /config/lists/youtube.json | jq --compact-output --raw-output '.[] | {id: .id, type: .type, name: .ChannelName, fullChannel: .fullChannel}' | while IFS= read -r line
            do
                CHANNELID=$(echo "${line}" | jq --raw-output '.id')
                CHANNELNAME=$(echo "${line}" | jq --raw-output '.name')
                CHANNELTYPE=$(echo "${line}" | jq --raw-output '.type')
                FULLCHANNEL=$(echo "${line}" | jq --raw-output '.fullChannel')
                PLAYLIST="false"
                log "Youtube" "-> Now downloading - Youtube: ${CHANNELNAME}" "youtube"
                log "Youtube" "--> Channel Type: ${CHANNELTYPE}" "youtube"
                log "Youtube" "--> Full Channel: ${FULLCHANNEL}" "youtube"
                if [[ "${FULLCHANNEL}" == "false" ]]; then
                    DATE_START="--dateafter now-1week"
                else
                    DATE_START=""
                fi
                if [[ "${CHANNELTYPE}" == "channel" && "${PLAYLIST}" == "false" ]]; then
                    log "Youtube" "youtube-dl https://www.youtube.com/channel/${CHANNELID}/videos" "youtube"
                    youtube-dl ${DATE_START} "https://www.youtube.com/channel/${CHANNELID}/videos" \
                        --config-location /home/abc/youtube-dl_youtube.txt \
                        -o '%(uploader)s/%(upload_date)s - %(title)s.%(ext)s' -4 \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                fi

                if [[ "${CHANNELTYPE}" == "user" && "${PLAYLIST}" == "false" ]]; then
                    log "Youtube" "youtube-dl https://www.youtube.com/user/${CHANNELID}/videos" "youtube"
                    youtube-dl ${DATE_START} "https://www.youtube.com/user/${CHANNELID}/videos" \
                        --config-location /home/abc/youtube-dl_youtube.txt \
                        -o '%(playlist_uploader)s/%(upload_date)s - %(title)s.%(ext)s' -4 \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                    
                fi
                if [[ "${CHANNELTYPE}" == "user" && "${PLAYLIST}" == "true" ]]; then
                    log "Youtube" "youtube-dl https://www.youtube.com/user/${CHANNELID}/playlists" "youtube"
                    youtube-dl ${DATE_START} "https://www.youtube.com/user/${CHANNELID}/playlists" \
                        --config-location /home/abc/youtube-dl_youtube.txt \
                        -o '%(playlist_uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -4 \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                    
                fi
                if [[ "${CHANNELTYPE}" == "channel" && "${PLAYLIST}" == "true" ]]; then
                    log "Youtube" "youtube-dl https://www.youtube.com/channel/${CHANNELID}/playlists" "youtube"
                    youtube-dl ${DATE_START} "https://www.youtube.com/channel/${CHANNELID}/playlists" \
                        --config-location /home/abc/youtube-dl_youtube.txt \
                        -o '%(playlist_uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -4 \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                    
                fi
            done
        else
            log "Youtube" "Nothing in list" "youtube"
        fi
        log "Youtube" "Done. Sleeping 30 min." "youtube"
        sleep 30m
    fi
    if [ "${TWITCH}" == "true" ]; then
        if [ -f /config/lists/twitch.json ]; then
            cat /config/lists/twitch.json | jq --compact-output --raw-output '.[] | {id: .id, vod: .vod, clips: .clips}' | while IFS= read -r line
            do
                DOWNLOAD_VOD=$(echo "${line}" | jq --raw-output '.vod')
                DOWNLOAD_CLIPS=$(echo "${line}" | jq --raw-output '.clips')
                CHANNELNAME=$(echo "${line}" | jq --raw-output '.id')
                if [ "${DOWNLOAD_VOD}" == "true" ]; then
                    log "Twitch VOD" "-> Now downloading - Twitch: ${CHANNELNAME} VODS" "twitch"

                    youtube-dl "https://www.twitch.tv/${CHANNELNAME}/videos/all" \
                        --config-location /home/abc/youtube-dl_twitch.txt \
                        --match-filter '!is_live & extractor = "twitch:vod"' \
                        -o '%(uploader)s/%(extractor_key)s/%(upload_date)s - %(id)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Twitch/$(dirname {}) && \
                    cp {} '${ENDDIR}'/Twitch/{} && \
                    rm -f {}'
                fi
                if [ "${DOWNLOAD_CLIPS}" == "true" ]; then
                    log "Twitch Clips" "-> Now downloading - Twitch: ${CHANNELNAME} CLIPS" "twitch"

                    log "Twitch Clips" "--> Grabbing Clip data...." "twitch"
                    /usr/local/bin/twitch-clips-grabber --clientID="ult4gydhjy5exy5euhcgzjx9q5t2bs" \
                        --channelName="${CHANNELNAME}" >batch.txt
                    log "Twitch Clips" "--> Done." "twitch"

                    youtube-dl -a batch.txt \
                        --config-location /home/abc/youtube-dl_twitch.txt \
                        --match-filter 'extractor = "twitch:clips"' \
                        -o '%(creator)s/%(extractor_key)s/%(upload_date)s - %(id)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Twitch/$(dirname {}) && \
                    cp {} '${ENDDIR}'/Twitch/{} && \
                    rm -f {}'

                    rm -f batch.txt
                fi
            done
        else
            log "Twitch" "Nothing in list" "twitch"
        fi
        log "Twitch" "Done. Sleeping for 3 hours." "twitch"
        sleep 3h
    fi
    if [ "${INSTAGRAM}" == "true" ]; then
        INSTAGRAM_USER=${INSTAGRAM_USER}
        INSTAGRAM_FOLLOWS=${INSTAGRAM_USER:-false}
        if [ -f /config/lists/instagram.json ]; then
            cat /config/lists/instagram.json | jq --compact-output --raw-output '.[] | {id: .id, profile: .profile, stories: .stories}' | while IFS= read -r line
            do
                IGUSERNAME=$(echo "${line}" | jq --raw-output '.id')
                IGPROFILE=$(echo "${line}" | jq --raw-output '.profile')
                IGSTORIES=$(echo "${line}" | jq --raw-output '.stories')
                if [ "${IGPROFILE}" == "true" ]; then
                    log "IG Profile" "-> Now downloading - Insta Profile: ${IGUSERNAME}" "instagram"
                    instaloader "${IGUSERNAME}" \
                        --dirname-pattern /data/instagram/{profile}/posts/ \
                        +/home/abc/args.txt
                    sleep 15s # It may help with GraphQL errors
                fi
                if [ "${IGSTORIES}" == "true" ]; then
                    log "IG Stories" "-> Now downloading - Insta Stories: ${IGUSERNAME}" "instagram"
                    instaloader "${IGUSERNAME}" \
                        --stories \
                        --no-posts \
                        --dirname-pattern /data/instagram/{profile}/stories/ \
                        --filename-pattern {date_utc:%d-%m-%Y}/{date_utc}_UTC \
                        +/home/abc/args.txt
                    sleep 15s # It may help with GraphQL errors
                fi
            done
        else
            log "IG" "Nothing in the list" "instagram"
        fi
        if [ "${INSTAGRAM_FOLLOWS}" == "true" ]; then
            log "IG Profile" "-> Now downloading - Followed Profiles" "instagram"
            instaloader "@${INSTAGRAM_USER}" \
                --dirname-pattern /data/instagram/{profile}/posts/ \
                +/home/abc/args.txt
            sleep 15s # It may help with GraphQL errors
            log "IG Stories" "-> Now downloading - Followed Stories" "instagram"
            instaloader :stories \
                --stories \
                --no-posts \
                --dirname-pattern /data/instagram/{profile}/stories/ \
                --filename-pattern {date_utc:%d-%m-%Y}/{date_utc}_UTC \
                +/home/abc/args.txt
            sleep 15s # It may help with GraphQL errors
        fi
        log "IG" "Done. Sleeping for 30 min." "instagram"
        sleep 30m
    fi
    if [ "${PORNHUB}" == "true" ]; then
        if [ -f /config/lists/pornhub.json ]; then
            cat /config/lists/pornhub.json | jq --compact-output --raw-output '.[] | {id: .id, type: .type}' | while IFS= read -r line
            do
                PHMODEL=$(cat "${line}" | jq '.id')
                log "Pornhub Model" "-> Now downloading - Pornhub: ${PHMODEL}" "pornhub"
                log "Pornhub Model" "--> Grabbing video urls" "pornhub"
                /usr/local/bin/pornhub-view-key-grabber --model="${PHMODEL}" >batch.txt
                log "Pornhub Model" "--> Done." "pornhub"
                /usr/local/bin/youtube-dl -q -a batch.txt \
                    -f bestvideo+bestaudio/best \
					--restrict-filename \
					--no-overwrites \
					--geo-bypass \
					--ignore-errors \
					--embed-subs \
					--all-subs \
					--convert-subs srt \
					--download-archive /config/lists/archive.txt \
					--write-description \
					--merge-output-format mkv \
					-o 'Pornhub/%(uploader)s/%(upload_date)s - %(title)s.%(ext)s' \
					--exec 'mkdir -p '${ENDDIR}'/XXX/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/XXX/$(dirname {}) && \
					rm -f $(dirname {})/*'
                rm -f batch.txt
            done
        else
            log "Pornhub" "Nothing in list" "pornhub"
        fi
        log "Pornhub" "Done. Sleeping for 3 hours." "pornhub"
        sleep 3h
    fi
    if [ "${DEEZER}" == "true" ]; then
        if [ -f /config/lists/deezer.txt ]; then
            cat /config/lists/deezer.txt | while IFS= read -r line
            do
                ARTISTID=${line// #.*/}
                ARTISTNAME=${line//.* #/}
                log "Deezer" "-> Now downloading - Deezer: ${ARTISTNAME}" "deezer"
                ./bin/SMLoadr-linux-x64 -q "FLAC" -p "${ENDDIR}/music/" "https://www.deezer.com/artist/${ARTISTID}"
            done
        fi
        log "Deezer" "Cleaning up.." "deezer"
        rm -fv downloadedSuccessfully.txt downloadedUnsuccessfully.txt downloadLinks.txt SMLoadr.log
        log "Deezer" "Done. Sleeping for 3 hours." "deezer"
        sleep 3h
    fi
done
