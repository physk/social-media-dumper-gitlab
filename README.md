# Social Media Dumper

---

Setup a folder for the lists to reside

```sh
mkdir -p /opt/appdata/social-media-dumper
```

Pull and run image

```sh
docker pull \
registry.gitlab.com/physk/social-media-dumper-gitlab && \
docker run -d \
--name social-dumper \
-v /opt/appdata/social-media-dumper:/config \
-v /mnt/unionfs/SocialMediaDumper:/data \
-e PUID=1000 \
-e PGID=1000 \
-e INSTAGRAM_USER=MY_INSTAGRAM_USER \
-e INSTAGRAM_FOLLOWS=true \
registry.gitlab.com/physk/social-media-dumper-gitlab
```

## Docker-compose

```yaml
social-dumper:
    container_name: social-dumper
    image: registry.gitlab.com/physk/social-media-dumper-gitlab
    environment:
      - "TZ=Europe/London"
      - "PUID=1000"
      - "PGID=1000"
      - "INSTAGRAM_USER=MY_INSTAGRAM_USER"
      - "INSTAGRAM_FOLLOWS=true"
    volumes:
      - "/opt/appdata/social-media-dumper:/config"
      - "/mnt/unionfs/SocialMediaDumper:/data"
```

## Enviroment Variables

| VAR | Default |
| ------ | ------ |
| PUID | 911 |
| PGID | 911 |
| DISABLE_YOUTUBE | false |
| DISABLE_TWITCH | false |
| DISABLE_INSTAGRAM | false |
| DISABLE_PORNHUB | false |
| INSTAGRAM_USER | false |
| INSTAGRAM_FOLLOWS | false |

## Add some content to the lists

### Youtube

```sh
docker exec -it social-dumper /app/add-content.sh -y
```

Example:

```sh
physk@SERVER ~ # docker exec -it social-dumper /app/add-content.sh -y
Enter channel id: UCN7gplqpfnrT-mxjoTR4ENg
Is this a channel or a user? [c/u]: c
What is the name of the channel?Tove Lo
Added Tove Lo to /config/lists/youtube.txt
```

### Twitch

```sh
docker exec -it social-dumper /app/add-content.sh -t
```

Example:

```sh
physk@SERVER ~ # docker exec -it social-dumper /app/add-content.sh -t
Enter channel id: shroud
Would you like to download VODs? [y/n]: y
Would you like to download Clips? [y/n]: y
Added shroud to /config/lists/twitch.txt
```

### Instagram

You will need to login before you can use the Instagram Dumper

```sh
docker exec -it social-dumper instaloader --login YOURUSERNAME --sessionfile /config/instagram_session.ini
```

This may not work Remotely, As Instgram will see it as a "Suspicious Activity".
If this is the case,
Run instaloader locally and upload the session file to your /config dir
(in the case of the base setup /opt/appdata/social-media-dumper)

```sh
docker exec -it social-dumper /app/add-content.sh -ig
```

Example:

```sh
physk@SERVER ~ # docker exec -it social-dumper /app/add-content.sh -ig
Enter page id: kittyplays
Added kittyplays to /config/lists/instagram.txt
```

### Pornhub

**NOTE: Only Currently works with model URL's not pornstar**
E.g. `https://www.pornhub.com/model/vivi-winters`

```sh
docker exec -it social-dumper /app/add-content.sh -p
```

Example:

```sh
physk@SERVER ~ # docker exec -it social-dumper /app/add-content.sh -p
Enter model username: vivi-winters
Added vivi-winters to /config/lists/pornhub.txt
```