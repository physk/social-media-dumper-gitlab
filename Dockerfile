# Copyright (c) 2019, PhysK
# All rights reserved.


# Lightweight Linux base
FROM alpine:latest
LABEL maintainer="PhysK <PhysK @ gitlab.physk.dev>"

# Some Args
ARG TWITCH_CLIPS_GRABBER=https://gitlab.physk.dev/social-media-dumper/twitch-clips-grabber/uploads/04b37c8961b3cc35c6f3aec9d6acac8c/twitch-clips-grabber-alpine.tar.gz
ARG PORHUB_VIEW_KEY_GRABBER=https://gitlab.physk.dev/social-media-dumper/pornhub-view-key-grabber/uploads/60b6f8b5f770b95c88dbe73d2826dd53/pornhub-view-key-grabber-alpine.tar.gz
ARG PHANTOMJS_VERSION=2.1.1

# Install certifacates, required dependencies
RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk update && apk upgrade && \
    apk add --no-cache \
        chromium@edge \
        nss@edge \
        freetype@edge \
        harfbuzz@edge \
        ttf-freefont@edge \
        libc6-compat \
        libstdc++ \
        libgcc \
        logrotate \
        shadow \
        bash \
        coreutils \
        ca-certificates \
        curl \
        ffmpeg \
        tar \
        jq

# Install PIP
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

# Install PhantomJS
RUN curl -Ls "https://github.com/dustinblackman/phantomized/releases/download/${PHANTOMJS_VERSION}/dockerized-phantomjs.tar.gz" | tar xz -C / && \
    curl -k -Ls https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-${PHANTOMJS_VERSION}-linux-x86_64.tar.bz2 | tar -jxvf - -C / && \
    cp phantomjs-${PHANTOMJS_VERSION}-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs && \
    rm -fR phantomjs-${PHANTOMJS_VERSION}-linux-x86_64

# InstalL s6 overlay
RUN wget https://github.com/just-containers/s6-overlay/releases/download/v1.21.4.0/s6-overlay-amd64.tar.gz -O s6-overlay.tar.gz && \
    tar xfv s6-overlay.tar.gz -C / && \
    rm -r s6-overlay.tar.gz

# Grab Latest Bins
RUN cd /tmp && \
    # wget ${TWITCH_CLIPS_GRABBER} && \
    # wget ${PORHUB_VIEW_KEY_GRABBER} && \
    # tar xfv twitch-clips-grabber-alpine.tar.gz && \
    # mv twitch-clips-grabber /usr/local/bin && \
    # tar xfv pornhub-view-key-grabber-alpine.tar.gz && \
    # mv pornhub-view-key-grabber /usr/local/bin && \
    # rm -f twitch-clips-grabber-alpine.tar.gz && \
    # rm -f pornhub-view-key-grabber.tar.gz && \
    curl --insecure -Lo /usr/local/bin/youtube-dl https://yt-dl.org/downloads/latest/youtube-dl && \
    chmod a+rx /usr/local/bin/youtube-dl && \
    /usr/local/bin/youtube-dl --update && \
    pip3 install instaloader

# Add user
RUN addgroup -g 911 abc && \
    adduser -u 911 -D -G abc abc

# Copy Root files
COPY root/ /

# Set WORKDIR
WORKDIR /config

# Add Volumes
VOLUME [ "/config" ]
VOLUME [ "/data" ]

ENTRYPOINT [ "/init" ]
